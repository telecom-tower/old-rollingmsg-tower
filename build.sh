#!/usr/bin/env bash

PROJECT_DIR=$(pwd)

echo "Working in: $PROJECT_DIR"

echo "Fetching ws2811 go library"
go get -d github.com/supcik-go/ws2811


echo "Fetching rpi_ws281x C library"
cd ${GOPATH}/src/github.com/supcik-go/ws2811
git clone https://github.com/jgarff/rpi_ws281x.git

echo "Patching code for cross compiling"
cd rpi_ws281x
rm -Rf golang
patch -i ${PROJECT_DIR}/01_cross.patch

echo "Building C library"
scons
cd ..

echo "Patching go code"
patch -i ${PROJECT_DIR}/02_go.patch

echo "Building tower"
export CGO_ENABLED=1
export GOOS=linux
export GOARCH=arm
export CC_FOR_TARGET=arm-linux-gnueabihf-gcc
export CC=arm-linux-gnueabihf-gcc
export CXX_FOR_TARGET=arm-linux-gnueabihf-g++
export CPATH=${GOPATH}/src/github.com/supcik-go/ws2811/rpi_ws281x
export LIBRARY_PATH=${GOPATH}/src/github.com/supcik-go/ws2811/rpi_ws281x

cd ${PROJECT_DIR}
go-wrapper download
go build

cd ${PROJECT_DIR}
echo "Building snap"
rm -Rf snapcraft
mkdir -p snapcraft/bin
mkdir -p snapcraft/meta
cp meta/snap.yaml snapcraft/meta
cp tower snapcraft/bin
cd snapcraft
snapcraft snap .

cd ${PROJECT_DIR}
cp snapcraft/*.snap .

echo "Done"